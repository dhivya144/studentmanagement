package com.main.Userdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.main.model.StudentDetailModel;
import com.main.model.UserModel;

public class UserDb {

	public UserModel checkUser(UserModel usermodel) {
		// TODO Auto-generated method stub
		Connection connection=null;
		PreparedStatement statememt = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection  = DriverManager.getConnection("jdbc:postgresql://localhost:5432/studentmanagement","postgres",
					"dhivya");
			statememt = connection.prepareStatement("select * from userDetails where username=? and password=?");
			statememt.setString(1, usermodel.getUserName());
			statememt.setString(2,usermodel.getPassword());
			statememt.execute();
			
			ResultSet resultset=statememt.executeQuery();
			
			while(resultset.next()) {
				return new UserModel(resultset);
			}
			
		} catch(Exception e) {
			System.out.println(e);
		} 
		return null;
	}



	public StudentDetailModel addStudent(StudentDetailModel studentdetailModel) throws SQLException {
		// TODO Auto-generated method stub
		Connection connection=null;
		PreparedStatement statement = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/studentmanagement","postgres",
					"dhivya");
			statement=connection.prepareStatement("insert into userdetails(name,dob,gender,email,phone_number,date_of_join,role_id,username,password,address) values(?,?,?,?,?,?,?,?,?,?)");  
			statement.setString(1, studentdetailModel.getName());
			statement.setString(2, studentdetailModel.getDob());
			statement.setString(3, studentdetailModel.getGender());
			statement.setString(4, studentdetailModel.getEmail());
			statement.setInt(5, studentdetailModel.getPhonenum());
			statement.setString(6, studentdetailModel.getDoj());
			statement.setInt(7, 3);
			statement.setString(8, studentdetailModel.getUserName());
			statement.setString(9, studentdetailModel.getPassword());
			statement.setString(10, studentdetailModel.getAddress());
			statement.executeUpdate();
			
			return studentdetailModel;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.close();
			}
		}

		return null;
	}
	
	

}
