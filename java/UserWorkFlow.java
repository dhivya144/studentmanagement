package com.main.workflow;

import com.main.Userdb.UserDb;
import com.main.model.UserModel;

public class UserWorkFlow {

	public   UserModel signInUser(UserModel usermodel) throws Exception {
		// TODO Auto-generated method stub
		UserDb userDb = new UserDb();
		UserModel dbUserModel = userDb.checkUser(usermodel);
		if(dbUserModel == null) {
			throw new Exception("User Not Found");
		}
		
		return dbUserModel;
	}

//
//	public UserModel addUser(UserModel usermodel) throws Exception {
//		// TODO Auto-generated method stub
//		UserDb userDb = new UserDb();
//		UserModel dbUserModel1 = userDb.addStudent(usermodel);
//		return dbUserModel1;
//		
//	}

}
