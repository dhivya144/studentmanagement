package com.main;

import java.io.BufferedReader;	
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.main.model.StudentDetailModel;
import com.main.model.UserModel;
import com.main.workflow.StudentDetailWorkFlow;
import com.main.workflow.UserWorkFlow;

/**
 * Servlet implementation class studentDetail
 */
//@WebServlet("/studentDetail")
public class StudentDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    PrintWriter writer = response.getWriter();
	    Cookie[] cookie = request.getCookies();

	      String name=""; 
	      for(Cookie cookies:cookie) {
	    	  if(cookies.getName().equals("name")) {
	    		  name=cookies.getValue();    
	    		  JSONObject obj=new JSONObject();
	    		  obj.put("name",name);
	    		  writer.print(obj);
	    	  }
	      }
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		StringBuilder stringBuilder = new StringBuilder();  
	    BufferedReader bufferedReader = null;  

	    try {  
	        InputStream inputStream = request.getInputStream(); 		    	
	        if (inputStream != null) {  
	        	InputStreamReader in=new InputStreamReader(inputStream);
	            bufferedReader = new BufferedReader(in);  

	            char[] charBuffer = new char[128];  
	            int bytesRead = -1;  

	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {  
	                stringBuilder.append(charBuffer,0, bytesRead);  
	            }  
	        } else {  
	            stringBuilder.append("");  
	        }  
	    } catch (IOException ex) {  
	    	System.out.println(ex);
	    } finally {  
	        if (bufferedReader != null) {  
	            try {  
	                bufferedReader.close();  
	            } catch (IOException ex) {  
			    	System.out.println(ex);
	            }  
	        }  
	    }  

	    String body = stringBuilder.toString();  
	    System.out.println(body);
	    
	   try
	   {
	    JSONParser parser = new JSONParser();  
	    JSONObject json = (JSONObject) parser.parse(body);  
		JSONObject detailObject = (JSONObject) json.get("studentdetail");
	
		StudentDetailModel studentdetailModel = new StudentDetailModel(detailObject);
		
		StudentDetailWorkFlow studentdetailworkflow = new StudentDetailWorkFlow();
		try {
			if((String) detailObject.get("name") != null &&(String) detailObject.get("dob") != null &&(String) detailObject.get("gender") != null &&(String) detailObject.get("email") != null &&(String) detailObject.get("num") != null &&(String) detailObject.get("doj") != null &&(String) detailObject.get("address") != null &&(String) detailObject.get("userName") != null && (String) detailObject.get("password") != null)
			{		
			StudentDetailModel dbUserModel = studentdetailworkflow.addStudentDetail(studentdetailModel);
			Cookie ck= new Cookie("name",String.valueOf(dbUserModel.getUserName()));
			response.addCookie(ck);
			JSONObject jsonObj= new JSONObject();
			jsonObj.put("studentdetail", dbUserModel.getStudentJson());
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().append(jsonObj.toString());
			
		}	
			}
		catch(Exception e) {
			response.setStatus(400);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().append("{error:invalid username/password}");
			e.printStackTrace();
		}			    
	   }
	   catch(ParseException e)
	   {
		   System.out.println(e);
	   }

//		doGet(request, response);
	}

}
