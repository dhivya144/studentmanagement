package com.main;

import com.main.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.main.model.UserModel;
import com.main.workflow.UserWorkFlow;

/**
 * Servlet implementation class demo
 */
//@WebServlet("/Demo")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public User() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub 
//      response.setContentType("application/json");	
	    PrintWriter writer = response.getWriter();
	    Cookie[] cookie = request.getCookies();

	      String name=""; 
	      for(Cookie cookies:cookie) {
	    	  if(cookies.getName().equals("name")) {
	    		  name=cookies.getValue();    
	    		  JSONObject obj=new JSONObject();
	    		  obj.put("name",name);
	    		  writer.print(obj);
	    	  }
	      }
	}
	
	
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//        response.setContentType("application/json");	
		
		 StringBuilder stringBuilder = new StringBuilder();  
		    BufferedReader bufferedReader = null;  

		    try {  
		        InputStream inputStream = request.getInputStream(); 		    	
		        if (inputStream != null) {  
		        	InputStreamReader in=new InputStreamReader(inputStream);
		            bufferedReader = new BufferedReader(in);  

		            char[] charBuffer = new char[128];  
		            int bytesRead = -1;  

		            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {  
		                stringBuilder.append(charBuffer,0, bytesRead);  
		            }  
		        } else {  
		            stringBuilder.append("");  
		        }  
		    } catch (IOException ex) {  
		    	System.out.println(ex);
		    } finally {  
		        if (bufferedReader != null) {  
		            try {  
		                bufferedReader.close();  
		            } catch (IOException ex) {  
				    	System.out.println(ex);
		            }  
		        }  
		    }  

		    String body = stringBuilder.toString();  
		    System.out.println(body);
		    
		   try
		   {
		    JSONParser parser = new JSONParser();  
		    JSONObject json = (JSONObject) parser.parse(body);  
			JSONObject detailObject = (JSONObject) json.get("user");
		
			UserModel usermodel = new UserModel(detailObject);
			
			UserWorkFlow userworkflow = new UserWorkFlow();
			try {
				if((String) detailObject.get("userName") != null && (String) detailObject.get("password") != null)
				{		
//				UserModel dbUserModel1 = userworkflow.signUpUser(usermodel);	
//			} else {
				UserModel dbUserModel = userworkflow.signInUser(usermodel);
				Cookie ck= new Cookie("name",String.valueOf(dbUserModel.getUserName()));
				response.addCookie(ck);
				JSONObject jsonObj= new JSONObject();
				jsonObj.put("user", dbUserModel.getJson());
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().append(jsonObj.toString());
				
			}	
				}
			catch(Exception e) {
				response.setStatus(400);
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().append("{error:invalid username/password}");
				e.printStackTrace();
			}			    
		   }
		   catch(ParseException e)
		   {
			   System.out.println(e);
		   }


//				doGet(request, response);

		}
}


	        
	      
	


