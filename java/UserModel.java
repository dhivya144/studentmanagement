package com.main.model;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.json.simple.JSONObject;


public class UserModel {
	private int id;
	private String name;
	private String dob;
	private String gender;
	private String email;
	private int phonenum;
	private String address;
	private String doj;
	private int roleid;
	private String userName;
	private String password;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhonenum() {
		return phonenum;
	}

	public void setPhonenum(int phonenum) {
		this.phonenum = phonenum;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}
		
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}						

	public UserModel(JSONObject detailObject) {
		// TODO Auto-generated constructor stub
		System.out.println(detailObject);
	if((String) detailObject.get("userName") != null) {
			System.out.println((String) detailObject.get("userName"));
			this.setUserName((String) detailObject.get("userName"));
		}		
	
	if((String) detailObject.get("password") != null) {
			System.out.println((String) detailObject.get("password"));
			this.setPassword((String) detailObject.get("password"));
			
	}	
		
	}



	public UserModel(ResultSet resultset) throws SQLException {
		try {
			this.setId(resultset.getInt("user_id"));
			this.setName(resultset.getString("name"));
			this.setDob(resultset.getString("dob"));
			this.setGender(resultset.getString("gender"));
			this.setEmail(resultset.getString("email"));
			this.setPhonenum(resultset.getInt("phone_number"));
			this.setDoj(resultset.getString("date_of_join"));
			this.setRoleid(resultset.getInt("role_id"));
			this.setUserName(resultset.getString("username"));
			this.setPassword(resultset.getString("password"));
			this.setAddress(resultset.getString("address"));			
		} 
		catch (Exception e) {
			System.out.println(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Object getJson() {
		// TODO Auto-generated method stub
		JSONObject jsonObject = new JSONObject();
//		jsonObject.put("id", this.getId());
		jsonObject.put("userName", this.getUserName());
		jsonObject.put("password", this.getPassword());
		System.out.println(jsonObject);
		return jsonObject;
	}



}
