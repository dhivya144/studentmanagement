package com.main.model;

import org.json.simple.JSONObject;

public class StudentDetailModel {
	private int id;
	private String name;
	private String dob;
	private String gender;
	private String email;
	private int phonenum;
	private String address;
	private String doj;
	private int roleid;
	private String userName;
	private String password;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhonenum() {
		return phonenum;
	}

	public void setPhonenum(int phonenum) {
		this.phonenum = phonenum;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}
		
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}						

	public StudentDetailModel(JSONObject detailObject) {
		// TODO Auto-generated constructor stub
		System.out.println(detailObject);
	if((String) detailObject.get("userName") != null) {
			System.out.println((String) detailObject.get("userName"));
			this.setUserName((String) detailObject.get("userName"));
		}		
	
	if((String) detailObject.get("password") != null) {
			System.out.println((String) detailObject.get("password"));
			this.setPassword((String) detailObject.get("password"));
			
	}
	if((String) detailObject.get("name") != null) {
		System.out.println((String) detailObject.get("name"));
		this.setName((String) detailObject.get("name"));
	}
			
if((String) detailObject.get("dob") != null) {
		System.out.println((String) detailObject.get("dob"));
		this.setDob((String) detailObject.get("dob"));
		
}
if((String) detailObject.get("gender") != null) {
	System.out.println((String) detailObject.get("gender"));
	this.setGender((String) detailObject.get("gender"));
}

if((String) detailObject.get("email") != null) {
	System.out.println((String) detailObject.get("email"));
	this.setEmail((String) detailObject.get("email"));
	
}
//
//int a =Integer.parseInt(detailObject.get("num").toString());
//System.out.println(a);
String num=((String)detailObject.get("num"));
System.out.println("Number value::"+Integer.parseInt(num));
//System.out.println(a);
//System.out.println(a);
//System.out.println(a);

if(Integer.parseInt(num) != 0) {	
	System.out.println(Integer.parseInt(num));
	this.setPhonenum(Integer.parseInt(num));
}

if((String) detailObject.get("doj") != null) {
	System.out.println((String) detailObject.get("doj"));
	this.setDoj((String) detailObject.get("doj"));
	
}
if((String) detailObject.get("address") != null) {
	System.out.println((String) detailObject.get("address"));
	this.setAddress((String) detailObject.get("address"));
}
	}

	public Object getStudentJson() {
		// TODO Auto-generated method stub
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("user_id", this.getId());
		jsonObject.put("name", this.getName());
		jsonObject.put("dob", this.getDob());
		jsonObject.put("gender", this.getGender());	
		jsonObject.put("email", this.getEmail());
		jsonObject.put("phone_number", this.getPhonenum());
		jsonObject.put("doj", this.getDoj());
		jsonObject.put("role_id", this.getRoleid());
		jsonObject.put("userName", this.getUserName());
		jsonObject.put("password", this.getPassword());
		jsonObject.put("address", this.getAddress());	
		System.out.println(jsonObject);
		return jsonObject;	}	
}
