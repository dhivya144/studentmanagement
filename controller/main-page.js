import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class MainPageController extends Controller {
  @action admin() {
    this.transitionToRoute('admin');
  }
}
