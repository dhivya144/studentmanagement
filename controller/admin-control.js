import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class AdminControlController extends Controller {
  @action dashBoard() {
    this.transitionToRoute('adminControl.dashboard');
  }
  @action studentDetails() {
    this.transitionToRoute('adminControl.studentDetails');
  }
  @action studentRecord(){
    this.transitionToRoute('adminControl.studentRecord');
  }
  @action teacherDetails(){
    this.transitionToRoute('adminControl.teacherDetails');
  }
  @action teacherRecord(){
    this.transitionToRoute('adminControl.teacherRecord');
  }
  @action subject(){
    this.transitionToRoute('adminControl.subject');
  }
  @action class(){
    this.transitionToRoute('adminControl.class');
  }
}
