import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AdminController extends Controller {
  @tracked userName = '';
  @tracked password = '';
  @tracked empty = '';
  @tracked pwdregx = false;
  @tracked namesregx = false;

  @action
  adminControl() {
    this.pwdregx = /^[A-Za-z0-9]{0,25}$/.test(this.password);
    this.namesregx = /^[A-Za-z]{0,25}$/.test(this.userName);
    this.Validation();
    if (
      this.userName != null &&
      this.password != null &&
      this.namesregx &&
      this.pwdregx
    ) {
      this.transitionToRoute('adminControl.dashboard');
    }
  }
  Validation() {
    if (this.userName == null && this.password == null) {
      this.empty = 'UserName or Password field is Empty';
    } else if (!this.namesregx || !this.pwdregx) {
      this.empty = 'Required Valid UserName or Password';
    }
  }
}
